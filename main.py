import argparse, re, json
from typing import Generator, List, Tuple, Optional

import torch
from transformers import BertTokenizer, AdamW
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import numpy as np
from hyperopt import fmin, hp, tpe
from tqdm.auto import tqdm

from model import NERTagger


class Defaults:
    batch_size = 8
    epochs_nb = 5
    hypertrain_epochs_nb = 25
    bert_model_type = "bert-base-cased"
    learning_rate = 0.00001


class Tags:
    tag_to_id = {
        "O": 0,
        "B-PER": 1,
        "I-PER": 2,
        "B-LOC": 3,
        "I-LOC": 4,
        "B-ORG": 5,
        "I-ORG": 6,
        "B-MISC": 7,
        "I-MISC": 8,
    }

    @classmethod
    def id_to_tag(cls):
        return {v: k for k, v in cls.tag_to_id.items()}


def prepare_datas(
    X: List[Tuple[str]], y: Optional[List[Tuple[str]]], tokenizer: BertTokenizer
) -> (torch.Tensor, torch.Tensor, Optional[torch.Tensor]):
    """prepare a batch of datas
    
    :param X: List of token sequences
    :param y: List of token sequences labels. None if no ground truth is provided
    :param tokenizer: a BertTokenizer
    :return: (new_X, attention_mask, new_y) as tensor. If param y was None,
        new_y will be None.
    """

    new_X, attention_mask = [], []
    new_y = [] if not y is None else None
    max_len = 0
    for i in range(len(X)):
        encoded_sent, sent_attention = (tokenizer.encode("[CLS]"), [1])
        if not new_y is None:
            sent_labels = [Tags.tag_to_id["O"]]

        for j, token in enumerate(X[i]):
            token_encoding = tokenizer.encode(token)
            encoded_sent += token_encoding
            sent_attention += [1] * len(token_encoding)
            if not new_y is None:
                sent_labels += [Tags.tag_to_id[y[i][j]]] * len(token_encoding)

        encoded_sent += tokenizer.encode("[SEP]")
        sent_attention.append(1)
        if not new_y is None:
            sent_labels.append(Tags.tag_to_id["O"])

        new_X.append(encoded_sent)
        attention_mask.append(sent_attention)
        if not new_y is None:
            new_y.append(sent_labels)
        if len(encoded_sent) > max_len:
            max_len = len(encoded_sent)

    # padding
    for encoded_sent, sent_attention in zip(new_X, attention_mask):
        encoded_sent += tokenizer.encode("[PAD]") * (max_len - len(encoded_sent))
        sent_attention += [0] * (max_len - len(sent_attention))
    if not new_y is None:
        for sent_labels in new_y:
            sent_labels += [Tags.tag_to_id["O"]] * (max_len - len(sent_labels))

    return (
        torch.tensor(new_X, dtype=torch.long),
        torch.tensor(attention_mask, dtype=torch.long),
        torch.tensor(new_y, dtype=torch.long) if not new_y is None else None,
    )


class Dataset:
    """class representing a CONNL-2003dataset from a file
    """

    def __init__(
        self, path: str, tokenizer: Optional[BertTokenizer],
    ):
        """ Dataset constructor
        
        :param path: path to a file containing datas in the CONNL-2003 format
        :param tokenizer: a BertTokenizer or None
        """
        raw_datas = open(path).read().strip()
        sents = raw_datas.split("\n\n")
        self.sents = []
        for sent in sents:
            self.sents.append([token.split(" ") for token in sent.split("\n")])
        self.sents = [list(zip(*sent)) for sent in self.sents]

        if tokenizer is None:
            self.tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")
        else:
            self.tokenizer = tokenizer

    def batch_nb(self, batch_size: int) -> int:
        return len(self.sents) // batch_size

    def count_tags(self, tokenizer) -> dict:
        count_tags_dict = {k: 0 for k in Tags.tag_to_id.keys()}
        for X, y in self.batches(1):
            X, attention_mask, y = prepare_datas(X, y, tokenizer)
            for i in range(y.shape[0]):
                for j in range(y.shape[1]):
                    count_tags_dict[Tags.id_to_tag()[y[i][j].item()]] += 1
        return count_tags_dict

    def batches(
        self, batch_size: int = Defaults.batch_size
    ) -> Generator[Tuple[List[str]], None, None]:
        """Simple batch generator

        :param loaded_datas: datas already formatted using the load_datas() function
        :param batch_size: size of the batch, in sequences
        """
        batch_nb = self.batch_nb(batch_size)
        for i in range(batch_nb):
            lidx = i * batch_size
            ridx = lidx + batch_size
            yield [sent[0] for sent in self.sents[lidx:ridx]], [
                sent[1] for sent in self.sents[lidx:ridx]
            ]


def train(
    model: torch.nn.Module,
    tokenizer: BertTokenizer,
    dataset: Dataset,
    device: torch.device,
    optimizer,
    loss_fn,
    batch_size: int = Defaults.batch_size,
    epochs_nb: int = Defaults.epochs_nb,
    verbose: bool = True,
):
    model.to(device)
    for epoch in range(epochs_nb):

        batch_nb = dataset.batch_nb(batch_size)
        if verbose:
            batches_progress = tqdm(
                dataset.batches(batch_size=batch_size), total=batch_nb, unit="batch"
            )
        else:
            batches_progress = dataset.batches(batch_size=batch_size)

        for X, y in batches_progress:
            model.train()
            optimizer.zero_grad()

            X, attention_mask, y = prepare_datas(X, y, tokenizer)
            X, attention_mask, y = X.to(device), attention_mask.to(device), y.to(device)

            # (batch_size, sequence_size)
            out = model(X, attention_mask=attention_mask)

            loss = loss_fn(out.permute(0, 2, 1), y)

            loss.backward()
            optimizer.step()

            if verbose:
                batches_progress.set_description(
                    "[e:{}][l:{:0.5f}]".format(epoch + 1, round(loss.item(), 4))
                )

    return model


def predict(
    model: NERTagger,
    tokenizer: BertTokenizer,
    sents: List[Tuple[str]],
    device: torch.device,
) -> List[List[Tuple[str]]]:

    model.to(device)
    model.eval()

    X, attention_mask, _ = prepare_datas(sents, None, tokenizer)
    X, attention_mask = X.to(device), attention_mask.to(device)
    preds = model(X, attention_mask)

    str_preds = []
    for i in range(preds.shape[0]):
        wpiece_offset = 0
        str_preds.append([])
        for j in range(len(sents[i])):
            str_preds[i].append(
                (
                    sents[i][j],
                    Tags.id_to_tag()[preds[i][j + 1 + wpiece_offset].max(0)[1].item()],
                )
            )
            wpiece_offset += len(tokenizer.tokenize(sents[i][j])) - 1
    return str_preds


def score(
    model: NERTagger,
    tokenizer: BertTokenizer,
    dataset: Dataset,
    device: torch.device,
    batch_size: int = Defaults.batch_size,
    verbose: bool = True,
) -> (float, float, float, float):
    """ Compute score metrics for model on specified dataset

    :param model: torch Module to test
    :param tokenizer: a BertTokenizer to prepare datas
    :param dataset: a Dataset
    :param device: a torch.device (cpu or gpu)
    :param batch_size:
    :param verbose: if true, print a loading bar
    :return: (accuracy, precision, recall, f1)
    """
    model.to(device)
    model.eval()
    accuracy_list, precision_list, recall_list = [], [], []

    with torch.no_grad():
        all_truth = []
        all_preds = []

        if verbose:
            batches_progress = tqdm(
                dataset.batches(batch_size=batch_size),
                total=dataset.batch_nb(batch_size),
                unit="batch",
            )
        else:
            batches_progress = dataset.batches(batch_size=batch_size)

        for X, y in batches_progress:
            X, attention_mask, y = prepare_datas(X, y, tokenizer)
            X, attention_mask, y = X.to(device), attention_mask.to(device), y.to(device)

            preds = model(X, attention_mask)

            for i in range(preds.shape[0]):  # for sent
                for j in range(preds.shape[1]):  # for token
                    # get the indice of max value
                    all_preds.append(preds[i][j].max(0)[1].item())
                    all_truth.append(y[i][j].item())

    accuracy = accuracy_score(all_truth, all_preds)
    precision = precision_score(all_truth, all_preds, average="macro")
    recall = recall_score(all_truth, all_preds, average="macro")
    f1 = f1_score(all_truth, all_preds, average="macro")

    return accuracy, precision, recall, f1


def weights_from_hyper_parameters(hyper_parameters: dict) -> List[float]:
    return list(
        zip(
            *sorted(
                [
                    (k, v)
                    for k, v in hyper_parameters.items()
                    if re.match(r"weight_.*", k)
                ],
                key=lambda e: Tags.tag_to_id[re.match(r"weight_(.*)", e[0]).group(1)],
            )
        )
    )[1]


def f1_objective(hyper_parameters: dict) -> float:
    weights = weights_from_hyper_parameters(hyper_parameters)

    model = NERTagger(len(list(Tags.tag_to_id)), Defaults.bert_model_type)
    tokenizer = BertTokenizer.from_pretrained(Defaults.bert_model_type)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    optimizer = AdamW(model.parameters(), lr=hyper_parameters["learning_rate"])
    loss_fn = torch.nn.CrossEntropyLoss(
        weight=torch.tensor(weights).to(device), reduction="mean"
    )

    train(
        model,
        tokenizer,
        Dataset("datas/train.txt", tokenizer),
        device,
        optimizer,
        loss_fn,
        batch_size=Defaults.batch_size,
        epochs_nb=Defaults.epochs_nb,
        verbose=False,
    )

    accuracy, precision, recall, f1 = score(
        model,
        tokenizer,
        Dataset("datas/valid.txt", tokenizer),
        device,
        batch_size=Defaults.batch_size,
        verbose=False,
    )

    return 1 - f1


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    arg_parser.add_argument(
        "-t",
        "--train",
        action="store_true",
        help="If specified, will train the model with the specified parameters",
    )
    arg_parser.add_argument(
        "-ht",
        "--hypertrain",
        action="store_true",
        help="If specified, will train the model several times to determine the best hyperparameters",
    )
    arg_parser.add_argument(
        "-s",
        "--score",
        action="store_true",
        help="If specified, will score the model models/model.pth",
    )
    arg_parser.add_argument(
        "-su",
        "--submit",
        action="store_true",
        help="""
            If specified, use the dataset found in datas/test-sample.txt and populate
            test-submit.txt with predicted tags for all tokens using models/model.pth
        """,
    )
    arg_parser.add_argument(
        "-p",
        "--predict",
        type=str,
        default=None,
        help="If specified with a string, use target model to predict specified tokens types",
    )
    arg_parser.add_argument(
        "-hpf",
        "--hyper-parameters-file",
        type=str,
        default=None,
        help="If provided, load hyperparameters from specified JSON (learning rate and class weights)",
    )
    arg_parser.add_argument(
        "-bz", "--batch-size", type=int, default=Defaults.batch_size, help="Batch size"
    )
    arg_parser.add_argument(
        "-en",
        "--epochs-nb",
        type=int,
        default=Defaults.epochs_nb,
        help="Number of epochs",
    )
    arg_parser.add_argument(
        "-lr",
        "--learning-rate",
        type=float,
        default=Defaults.learning_rate,
        help="Learning rate",
    )
    arg_parser.add_argument(
        "-bt",
        "--bert-model-type",
        type=str,
        default=Defaults.bert_model_type,
        help="Bert model type (ex: bert-model-cased)",
    )
    args = arg_parser.parse_args()

    model = NERTagger(len(list(Tags.tag_to_id)), args.bert_model_type)
    tokenizer = BertTokenizer.from_pretrained(args.bert_model_type)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if args.train:

        train_dataset = Dataset("datas/train.txt", tokenizer)

        if args.hyper_parameters_file:
            hyper_parameters = json.loads(open(args.hyper_parameters_file).read())
            learning_rate = hyper_parameters["learning_rate"]
            weights = weights_from_hyper_parameters(hyper_parameters)
        else:
            learning_rate = args.learning_rate
            frequencies = [
                count
                for tag, count in sorted(
                    list(train_dataset.count_tags(tokenizer).items()),
                    key=lambda e: Tags.tag_to_id[e[0]],
                )
            ]
            weights = [max(frequencies) / f if f != 0 else 1 for f in frequencies]

        print(
            "training with hyperparameters :\nclass weights : {}\nlearning rate : {}".format(
                weights, learning_rate
            )
        )
        loss_fn = torch.nn.CrossEntropyLoss(
            weight=torch.tensor(weights, dtype=torch.float).to(device), reduction="mean"
        )

        optimizer = AdamW(model.parameters(), lr=learning_rate)

        model = train(
            model,
            tokenizer,
            train_dataset,
            device,
            optimizer,
            loss_fn,
            batch_size=args.batch_size,
            epochs_nb=args.epochs_nb,
        )
        torch.save(model.state_dict(), "models/model.pth")
        exit()

    if args.hypertrain:
        tokenizer = BertTokenizer.from_pretrained("bert-base-cased")
        train_dataset = Dataset("datas/train.txt", tokenizer)

        frequencies = [
            count
            for tag, count in sorted(
                list(train_dataset.count_tags(tokenizer).items()),
                key=lambda e: Tags.tag_to_id[e[0]],
            )
        ]
        weights = [max(frequencies) / f if f != 0 else 1 for f in frequencies]

        space = {
            "learning_rate": hp.uniform(
                "learning_rate",
                Defaults.learning_rate / 10,
                Defaults.learning_rate * 10,
            ),
        }
        for i, weight in enumerate(weights):
            str_id = "weight_" + Tags.id_to_tag()[i]
            space[str_id] = hp.normal(str_id, weight, 5)

        best_parameters = fmin(
            fn=f1_objective,
            space=space,
            algo=tpe.suggest,
            max_evals=Defaults.hypertrain_epochs_nb,
        )

        print("best hyper-parameters : {}".format(best_parameters))
        print("saving hyper-parameters in hyparameters.json")
        open("hyperparameters.json", "w").write(json.dumps(best_parameters))
        exit()

    if args.score:
        score_dataset = Dataset("datas/valid.txt", tokenizer)
        model.load_state_dict(
            torch.load("models/model.pth", map_location=device), strict=False
        )
        accuracy, precision, recall, f1 = score(
            model, tokenizer, score_dataset, device, batch_size=args.batch_size
        )
        print(
            "accuracy  : {}\nprecision : {}\nrecall    : {}\nf1-score  : {}".format(
                accuracy, precision, recall, f1
            )
        )
        exit()

    if args.submit:
        model.load_state_dict(
            torch.load("models/model.pth", map_location=device), strict=False
        )

        test_dataset = Dataset("datas/test-sample.txt", tokenizer)
        submit_file = open("datas/test-submit.txt", "a+")
        submit_file.truncate(0)  # empty the file

        batch_nb = test_dataset.batch_nb(1)
        batches_progress = tqdm(test_dataset.batches(batch_size=1), total=batch_nb)

        for X, _ in batches_progress:
            predictions = predict(model, tokenizer, X, device)
            for prediction in predictions:
                for tup in prediction:
                    submit_file.write(tup[0] + " " + tup[1] + "\n")
                submit_file.write("\n")

        submit_file.close()
        exit()

    if not args.predict is None:
        model.load_state_dict(
            torch.load("models/model.pth", map_location=device), strict=False
        )
        predictions = predict(model, tokenizer, [args.predict.split(" ")], device)
        print("prediction : {}".format(predictions))

