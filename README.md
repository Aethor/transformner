---
title: "Named Entity Recognition in the BERT era : TransformNER"
author: Arthur Amalvy
---

*in this report, any expression in bold is a clickable link*

\newpage

# Introduction

Named Entity Recognition is one of the oldest and most fondamentals task in NLP : Given an input list of tokens, the task is to find which tokens refers to an entity, and for all those tokens, to find the category of that entity.

The CONLL-2003 shared task is the classic example of Named Entity Recognition. Here, four types of entities have to be distingished : persons, locations, organisations, and miscellanous. You can access to an overview of current results on this task [**here**](http://nlpprogress.com/english/named_entity_recognition.html). Here, we can see that plain *BERT* achieves almost state-of-the-art results (92.4 F1 score for *bert-base*, 92.8 F1 score for *bert-large*), which is impressive considering it aims to be a rather generalistic model.

Working on CONLL-2003, we therefore decided to use BERT to see if we could reproduce the results indicated here, but also to learn how to use this powerful model. We used the *transformers* implementation from *huggingface*.

The code is available on [**gitlab**](https://gitlab.com/Aethor/transformner), and you can also copy the following [**Colab Notebook**](https://colab.research.google.com/drive/1tJxzt0x0hyaXRuEze2OLcL93MZtqHQoR) to train the model using a GPU (please note that the whole code is compatible with GPU usage).


# Quickstart

To train a model, simply launch :
```sh
python3 main.py --train
```

The model will be trained on the *datas/train.txt* file with the default parameters and saved as *models/model.pth*. You can then score the model on the validation set (*datas/valid.txt*) using `python3 main.py --score`. 

If you want to finetune hyperparameters, and then use those finetuned parameters for training, you can use the `--hypertrain` flag. This will compute the best parameters and put them in the *hyperparameters.json* file (please be aware that this might take a *very* long time). You can then use this file when training like so :

```sh
python3 main.py --train --hyper-parameters-file hyperparameters.json
```

To use the current model to submit results in test-submit.txt, use `python3 main.py --submit`. Please note the current content of *test-submit.txt* contains the result of the best model we could train.


# Model

We use the pretrained *bert-base-cased* model available in the huggingface *transformers* library, and fine-tune it to our NER task using the CONLL-2003 datas.

We first feed a batch of sentence to the pretrained Bert model, that is used to generate an hidden state per token. Those hidden states are then given to a classifier layer that gives a log-probability to each class of each token. Here is the corresponding pytorch module :

```python
class NERTagger(torch.nn.Module):
    def __init__(self, tags_nb: int, bert_model_type: str):
        """NERTagger init method
        
        :param tags_nb: number of possible tags
        :param bert_model_type: bert model type
        """
        super(NERTagger, self).__init__()
        self.bert = BertModel.from_pretrained(bert_model_type)
        self.linear = torch.nn.Linear(self.bert.config.hidden_size, tags_nb)

    def forward(
        self, sequences: torch.Tensor, attention_mask: torch.Tensor
    ) -> torch.Tensor:
        """NERTagger forward pass
        
        :param sequences: (batch_size, seq_size)
        :param attention_mask: (batch_size, seq_size)
        :return: (batch_size, seq_size)
        """
        # (batch_size, seq_size, hidden_size)
        hidden = self.bert(sequences, attention_mask=attention_mask)[0]

        # (batch_size, seq_size, tags_nb)
        out = self.linear(hidden)

        return out
```

# Loading datas for BERT

Fortunately, the datas for CONLL-2003 are very straightforward. Here is an exemple raw sentence from the dataset :

```
EU B-ORG
rejects O
German B-MISC
call O
to O
boycott O
British B-MISC
lamb O
. O
```

However, as we're using BERT, we have to tokenize our datas in a specific way. BERT uses WordPiece tokenized sentences as inputs, and also need special tokens to indicates start and end of a sequence, and padding tokens. For example, the above example looks like this after tokenization :

```python
['[CLS]', 'EU', 'rejects', 'German', 'call', 'to', 'boycott', 'British',
    'la', '##mb', '.', '[SEP]', '[PAD]', '[PAD]']
```

We also need to compute an attention mask to indicate our BERT model where are padding tokens. In this case, it would be :

```python
[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0]
```

for more details on data preparation for BERT, please refer to the *prepare_datas* function in *main.py*, the [**BERT**](https://arxiv.org/abs/1810.04805) paper or the [**huggingface documentation**](https://huggingface.co/transformers/model_doc/bert.html#berttokenizer).


# Training

We use the classic Adam optimizer, cross-entropy to mesure our loss when training, and use 5 epochs for training (the BERT authors recommend between 3 and 5 epochs of fine-tuning). Because we are dealing with an unbalanced dataset, we also weight classes in our cross-entropy loss according to the following formula :

$$w_i = \frac{max_i(card(T))}{card(t_i)}$$

where : 

* $w_i$ is the weight for the tag $i$
* $T$ is the set of all tags
* $card(t_i)$ is the number of time the tag $i$ appears in the training set

![Initial weights of every tag](assets/initial_weights.png)

\newpage

## Hyperparameters optimization

Aside of the provided training datas, we also have validation datas that we can use to finetune our hyperparameters. Our aim here is to maximize our F1-score.

To do so, we use the [**Hyperopt**](https://github.com/hyperopt/hyperopt) library, which allows us to use techniques more powerful than a classic random search or grid search (see [**Tree Parzen Estimator**](https://papers.nips.cc/paper/4443-algorithms-for-hyper-parameter-optimization.pdf)).

We decide to finetune our learning rate and our class weights. Hyperopt allows use to define a search space for each hyperparameter, which consists in a probability distribution. 

* We set our learning rate search space to be a continuous distribution between 10e-4 ans 10e-6
* We set each tag weight distribution to be a normal distribution centered around the original computed weight, with standard deviation 5

![Weights search space](assets/weights_space.png)

We set our hyper-loss function to be $1 - f1$, and try 25 iterations of hyper-optimisation (we would have done more, however the process is very costly since the model has to be retrained at every iteration).


# Final results

Here are the (macro-averaged) scores on the validation set reported before fine tuning hyperparameters, using a learning rate of 10e-5 and our pre-computed class weights :

| accuracy | precision | recall | f1-score |
| -------- | --------- | ------ | -------- |
|  98.87%  | 88.96%    | 93.72% |   91.16% |


After fine-tuning the parameters, we see an improvement of more than 1 point in our f1 score, at the cost of a bit of recall :

| accuracy | precision | recall | f1-score |
| -------- | --------- | ------ | -------- |
|  99.11%  |   92.96%  | 92.24% | 92.55%   |


# Conclusion

Considering other published results (the state-of-the-art f1 score being 93.5), ours are pretty encouraging. We were able to learn more about BERT, and learnt hyper-parameters optimization, while showing it was a valuable technique.


# References

* Devlin et al. 2018. *Bert : Pre-Training of Deep Bidirectional Transformers for Language Understanding*
* J. Bergstra, R. Bardenet, Y. Bengio, B. Kgl. 2011. *Algorithms for Hyper-Parameter Optimization*