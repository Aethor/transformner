import torch
from transformers import BertModel


class NERTagger(torch.nn.Module):
    def __init__(self, tags_nb: int, bert_model_type: str):
        """NERTagger init method
        
        :param tags_nb: number of possible tags
        """
        super(NERTagger, self).__init__()
        self.bert = BertModel.from_pretrained(bert_model_type)
        self.linear = torch.nn.Linear(self.bert.config.hidden_size, tags_nb)

    def forward(
        self, sequences: torch.Tensor, attention_mask: torch.Tensor
    ) -> torch.Tensor:
        """NERTagger forward pass
        
        :param sequences: (batch_size, seq_size)
        :param attention_mask: (batch_size, seq_size)
        :return: (batch_size, seq_size)
        """
        # (batch_size, seq_size, hidden_size)
        hidden = self.bert(sequences, attention_mask=attention_mask)[0]

        # (batch_size, seq_size, tags_nb)
        out = self.linear(hidden)

        return out
